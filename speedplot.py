#!/usr/bin/env python3

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

import miserele


def sample_log_states(n, p, samples=5, planted=None):
    vals = []
    for _i in range(samples):
        if planted is not None:
            other_edges = max(int(p * 0.5 * (n ** 2 - planted * (n / planted) ** 2)), 0)  # p ~ inter-clique
            # other_edges = max(int(p * 0.5 * n ** 2 - planted * 0.5 * (n / planted) ** 2), 0) # p ~ total
            G = miserele.planted_mis_graph(n, planted, other_edges)
        else:
            G = nx.generators.random_graphs.gnp_random_graph(n, p)
        r = miserele.MISProblemBest(G, 0).expand()
        vals.append(r.states)
        print(n, p, r)
    return vals


if __name__ == '__main__':
    sizes = np.arange(10, 71, 2)
    dens = np.arange(0.15, 0.41, 0.05)
    vals = np.zeros((len(sizes), len(dens)))
    valsp = np.zeros((len(sizes), len(dens)))
    for si, s in enumerate(sizes):
        for di, d in enumerate(dens):
            vals[si, di] = np.log2(np.median(sample_log_states(s, d, samples=10, planted=None)))
            valsp[si, di] = np.log2(np.median(sample_log_states(s, d, samples=10, planted=9)))

    fig, ax = plt.subplots()
    lv = ax.plot(vals, '-')
    lp = ax.plot(valsp, '--')
    # ax.set_xticklabels(np.round(dens, 3))
    ax.set_xticklabels(sizes)
    # ax.set_xticks(np.arange(len(dens)))
    ax.set_xticks(np.arange(len(sizes)))
    ax.set_xlabel("$n$")
    ax.set_ylabel("$log_2(BS)$")
    plt.title("$log_2(branching\\ states)$")
    fig.legend(lv + lp,
               ["Gnp p={:.3f}".format(d) for d in dens] +
               ["Planted(9) p={:.3f}".format(d) for d in dens],
               'upper left')
    fig.tight_layout()
    plt.show()
