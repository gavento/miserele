#!/usr/bin/env python3

import time
import copy


class MISProblemBase:
    """
    Maximum independent set branching computation.
    """

    class Result:
        def __init__(self, states, opt, solution=None, time=0.0):
            self.states = states
            self.opt = opt
            self.solution = solution if solution is not None else []
            self.time = time

        def __repr__(self):
            return "<Result opt={}, states={} in {:.2f} s>".format(self.opt, self.states, self.time)

    def __init__(self, G, lower_bound=None):
        self.G = G
        self.lower_bound = lower_bound
        if self.lower_bound is not None and self.lower_bound < 0:
            self.lower_bound = 0

    def copy(self):
        "A *shallow* copy of the state (shares e.g. the graph instance)."
        return copy.copy(self)

    def simplify(self):
        """
        A method that may simplify the instance with a rule.
        If a simplification takes place, calls expand on the new state and returns an agmented Result.
        Returns None if no simplification takes place.
        """
        return None

    def upper_bound(self):
        """
        A method that may return any upper bound on the optimum of this (sub)problem.
        May return None.
        """
        return None

    def subproblem_helper(self, deleted, sol):
        """
        Helper that creates subinstances by deleting some vertices and adding some to the solution.
        """
        new = self.copy()
        new.G = self.G.copy()
        if self.lower_bound is not None:
            new.lower_bound = max(self.lower_bound - len(sol), 0)
        new.G.remove_nodes_from(deleted)
        return new

    def get_branchings(self):
        """
        Return the branching option in the order to be explored.
        Returns [(subproblem, solution_extension)]
        """
        v = self.get_branching_vertex()
        return [
            (self.subproblem_helper([v], []), []),
            (self.subproblem_helper([v] + list(self.G.neighbors(v)), [v]), [v]),
        ]

    def get_branching_vertex(self):
        """
        Return a vertex to branch on. May be used by `get_branchings`.
        """
        return list(self.G)[0]

    def expanded_hook(self, best):
        """
        Called after the expansion of self is finished with `best` Result.
        """
        pass

    def expand(self):
        """
        Run the computation on the current state with optional lower bound.
        """
        t0 = time.clock()

        # Basic cases
        if len(self.G.nodes) == 0:
            return self.Result(1, 0, [], time.clock() - t0)
        if len(self.G.nodes) == 1:
            return self.Result(1, 1, [list(self.G)[0]], time.clock() - t0)

        # Try to simplify
        sr = self.simplify()
        if sr is not None:
            return sr

        # Try to compute a solution upper bound
        if self.lower_bound is not None:
            ub = self.upper_bound()
            if ub is not None and ub <= self.lower_bound:
                return self.Result(1, 0, None, time.clock() - t0)

        # Select a branching vertex and branches to try
        states = 1
        best = self.Result(1, 0, [])
        for inst, sol_part in self.get_branchings():
            if self.lower_bound is not None:
                inst.lower_bound = max(0, self.lower_bound - len(sol_part))
            r = inst.expand()
            states += r.states
            r.opt += len(sol_part)
            r.solution += sol_part
            if best.opt < r.opt:
                best = r
                # only update if lower bound is desired
                if self.lower_bound is not None and self.lower_bound < r.opt:
                    self.lower_bound = r.opt

        best.states = states
        best.time = time.clock() - t0
        self.expanded_hook(best)
        return best
