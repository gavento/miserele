#!/usr/bin/env python3

import networkx as nx
from tnetbase import argmax

from .base import MISProblemBase


class BranchMaxDegree:
    def get_branching_vertex(self):
        return argmax(self.G.nodes, lambda v: self.G.degree[v])


class SimplifyComps:
    def simplify(self):
        Cs = list(nx.connected_components(self.G))
        if len(Cs) <= 1:
            return None
        s = self.Result(0, 0, [])
        for C in Cs:
            G2 = self.G.subgraph(C)
            s2 = self.copy()
            if self.lower_bound is not None:
                s2.lower_bound = 0  # TODO: may be better if other comps are small
            s2.G = G2
            r = s2.expand()
            s.opt += r.opt
            s.solution += r.solution
            s.states += r.states
        return s


class SimplifyCompsLeaves(SimplifyComps):
    def simplify(self):
        r = super().simplify()
        if r is not None:
            return r

        for v in self.G:
            if self.G.degree[v] == 1:
                s = self.subproblem_helper([v] + list(self.G.neighbors(v)), [v]).expand()
                s.opt += 1
                s.solution += [v]
                s.states += 1
                return s

        return None


class SimplifyCompsLeavesDoms(SimplifyCompsLeaves):
    def simplify(self):
        r = super().simplify()
        if r is not None:
            return r

        G = self.G
        for v in G:
            for u in G.neighbors(v):
                if G.degree[u] <= G.degree[v]:
                    if all(G.has_edge(w, v) or w == v for w in G.neighbors(u)):
                        s = self.subproblem_helper([v], []).expand()
                        s.states += 1
                        return s

        return None


class BoundSize:
    def upper_bound(self):
        return self.G.order()


class BoundMaximalMatching:
    def upper_bound(self):
        mm = nx.algorithms.matching.maximal_matching(self.G)
        return self.G.order() - len(mm)


class BranchRemoveMirrors:
    def is_clique(self, vertices):
        for v in vertices:
            for w in vertices:
                if w != v and not self.G.has_edge(v, w):
                    return False

        return True

    def mirrors(self, v):
        two_neighborhood = set()
        for w in self.G.neighbors(v):
            two_neighborhood.union(self.G.neighbors(w))

        mirrors = []

        for w in two_neighborhood:
            if self.is_clique(set(self.G.neighbors(v)).difference(set(self.G.neighbors(w)))):
                mirrors.append(w)

        return mirrors

    def get_branchings(self):
        v = self.get_branching_vertex()
        return [
            (self.subproblem_helper([v] + self.mirrors(v), []), []),
            (self.subproblem_helper([v] + list(self.G.neighbors(v)), [v]), [v]),
        ]


class MISProblemSimple(BranchMaxDegree, SimplifyComps, BoundSize, MISProblemBase):
    pass


class MISProblemMed(BranchMaxDegree, SimplifyCompsLeavesDoms, BoundMaximalMatching, MISProblemBase):
    pass


class MISProblemBest(BranchRemoveMirrors, BranchMaxDegree, SimplifyCompsLeavesDoms, BoundMaximalMatching,
                     MISProblemBase):
    pass
