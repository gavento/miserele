#!/usr/bin/env python3

import numpy as np
import tensorflow as tf

from tnetbase import GCNet


def log2(x):
    return tf.log(x) / np.log(2)


class GCRLNet(GCNet):

    class OrderStat:
        eval = 0
        train = 0

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.stats = {}

    def order_stat(self, order):
        return self.stats.setdefault(order, self.OrderStat())

    def edge_gadget(self, g_layer, e_layer, v_in, v_out, dim=None):
        layer = tf.concat([
            tf.tile(g_layer, [self.g_m, 1]),
            v_in,
            v_out,
        ], axis=1)
        layer = tf.layers.dense(layer, dim, activation=tf.nn.relu)
        return layer

    def vertex_gadget(self, g_layer, v_layer, v_sum, v_mean, v_max, dim=None):
        layer = tf.concat([
            tf.tile(g_layer, [self.g_n, 1]),
            v_layer,
            v_sum,
            v_mean,
            v_max,
        ], axis=1)
        layer = tf.layers.dense(layer, dim, activation=tf.nn.relu)
        layer = tf.layers.dense(layer, dim, activation=tf.nn.relu)
        return layer

    def graph_gadget(self, g_layer, v_layer, dim=None):
        layer = tf.concat([
            g_layer,
            tf.reduce_sum(v_layer, axis=0, keepdims=True),
            tf.reduce_max(v_layer, axis=0, keepdims=True),
            tf.reduce_mean(v_layer, axis=0, keepdims=True),
        ], axis=1)
        layer = tf.layers.dense(layer, dim, activation=tf.nn.relu)
        layer = tf.layers.dense(layer, dim, activation=tf.nn.relu)
        return layer

    def construct(self, **kwargs):
        super().construct(**kwargs)
        with self.session.graph.as_default():
            self.t_v = tf.placeholder(tf.int32, [], name='t_v')
            self.t_a = tf.placeholder(tf.int32, [], name='t_a')
            self.t_return = tf.placeholder(tf.float32, [], name='t_return')

            # Vertex selection
            self.v_logits = tf.squeeze(tf.layers.dense(self.v_layer, 1), axis=1)
            self.v_entropy = tf.distributions.Categorical(logits=self.v_logits).entropy()
            self.v_probs = tf.nn.softmax(self.v_logits)

            # Action selection
            self.a_logits = tf.reshape(tf.layers.dense(self.v_layer, self.args.actions), [-1])
            self.a_entropy = tf.distributions.Categorical(logits=self.a_logits).entropy()
            self.a_probs = tf.nn.softmax(self.a_logits)

            # Global baseline
            self.baseline = tf.squeeze(tf.layers.dense(self.g_layer, 1), axis=1)

            # Training
            self.loss_v = 0.0
            self.loss_a = tf.losses.sparse_softmax_cross_entropy(
                self.t_a, self.a_logits,
                weights=(self.t_return - tf.stop_gradient(self.baseline)))
            self.loss_baseline = tf.losses.mean_squared_error([self.t_return], self.baseline)
            self.loss = self.loss_v + self.loss_baseline + self.loss_a

            with self.summary_context():
                self.summaries.extend([
                    tf.contrib.summary.scalar("train/loss_v", self.loss_v),
                    tf.contrib.summary.scalar("train/loss_a", self.loss_a),
                    tf.contrib.summary.scalar("train/loss_baseline", self.loss_baseline),
                    tf.contrib.summary.scalar("train/entropy_a_norm", self.a_entropy),
                    tf.contrib.summary.scalar("train/return_per_v", self.t_return / self.g_n_f),
                    tf.contrib.summary.scalar("train/baseline_per_v", self.baseline / self.g_n_f),
                    tf.contrib.summary.scalar("train/diff_per_v", (self.t_return - self.baseline) / self.g_n_f),
                    # tf.contrib.summary.histogram("train/a_probs", tf.reduce_mean self.a_probs),
                ])
                self.states_best = tf.placeholder(tf.float32, [], name='states_best')
                self.states_rl = tf.placeholder(tf.float32, [], name='states_rl')
                self.missed = tf.placeholder(tf.float32, [], name='missed')
                self.test_summaries = [
                    tf.contrib.summary.scalar("test/missed", self.missed),
                    tf.contrib.summary.scalar("test/log_states_best_per_v", log2(self.states_best) / self.g_n_f),
                    tf.contrib.summary.scalar("test/log_states_rl_per_v", log2(self.states_rl) / self.g_n_f),
                    tf.contrib.summary.scalar(
                        "test/log_states_rl_better_per_v",
                        (- log2(self.states_rl) + log2(self.states_best)) / self.g_n_f),
                ]
