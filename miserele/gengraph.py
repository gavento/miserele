#!/usr/bin/env python3

import networkx as nx
import random


def planted_mis_graph_sizes(sizes, other_edges, seed=None):
    """
    Return a graph with planted solution of size `len(sizes)`.
    This is done by creating cliques of sizes `sizes`,
    pre-selecting a solution set one in each clique and adding
    additional edges without breaking the solution.
    The vertex labels are randomly permuted, fails if other_edges can't be added.
    """
    rng = random.Random(seed)
    G = nx.Graph()
    sol = set()

    vs = list(range(sum(sizes)))
    rng.shuffle(vs)
    for s in sizes:
        assert len(vs) >= s
        G = nx.algorithms.operators.binary.union(
            G,
            nx.generators.classic.complete_graph(vs[:s]))
        sol.add(rng.choice(vs[:s]))
        vs = vs[s:]
    assert len(vs) == 0

    es = [(u, v) for u in G for v in G if u != v]
    rng.shuffle(es)
    for u, v in es:
        if other_edges <= 0:
            break
        if not G.has_edge(u, v) and not (u in sol and v in sol):
            G.add_edge(u, v)
            other_edges -= 1
    # assert other_edges == 0
    return G


def planted_mis_graph(n, opt, other_edges, seed=None):
    """
    Same as `planted_mis_graph_sizes` with almost-uniform clique sizes.
    """
    sizes = [n // opt for _ in range(opt)]
    for i in range(n % opt):
        sizes[i] += 1
    assert sum(sizes) == n
    return planted_mis_graph_sizes(sizes, other_edges, seed)
