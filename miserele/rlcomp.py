#!/usr/bin/env python3

import numpy as np
from tnetbase import argmax

from . import base, variants


class MISRL(base.MISProblemBase):

    def __init__(self, net, G, lower_bound=None):
        super().__init__(G, lower_bound=lower_bound)
        self.net = net

    def get_branching_vertex(self):
        raise NotImplementedError()

    def get_action(self):
        self.net.order_stat(self.G.order()).eval += 1
        feed = self.net.make_feed(self.G)
        a_probs = self.net.compute(feed, ret=[self.net.a_probs])[0]
        assert len(a_probs) == self.net.args.actions * self.G.order()
        return np.random.choice(len(a_probs), p=a_probs)

    def get_branchings(self):
        if self.G.order() >= self.net.args.min_net_vertices:
            self.chosen_a_idx = self.get_action()
            v = list(self.G.nodes())[self.chosen_a_idx // self.net.args.actions]
            a = self.chosen_a_idx % self.net.args.actions
            assert v in self.G.nodes
        else:
            self.chosen_a_idx = None
            a = 0
            v = argmax(self.G.nodes, lambda v: self.G.degree[v])

        brs = [
            (self.subproblem_helper([v], []), []),
            (self.subproblem_helper([v] + list(self.G.neighbors(v)), [v]), [v]),
        ]
        if a == 1:
            brs.reverse()
        if a == 2:
            brs = brs[:1]
        if a == 3:
            brs = brs[1:]
        assert len(brs) >= 1
        return brs

    def expanded_hook(self, best):
        if self.chosen_a_idx is None:
            return
        if np.random.random() < self.net.args.train_fraction:
            self.net.order_stat(self.G.order()).train += 1
            feed = self.net.make_feed(self.G)
            feed[self.net.t_a] = self.chosen_a_idx
            feed[self.net.t_return] = -np.log2(best.states)
            if self.net.args.actions > 2:
                opt = variants.MISProblemBest(self.G, lower_bound=best.opt).expand()
                feed[self.net.t_return] -= (opt.opt - best.opt) * self.net.args.penalty
            assert self.rate > 1e-10
            self.net.train(feed, ret=[], rate=self.rate)
