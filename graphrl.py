#!/usr/bin/env python3

import networkx as nx
import random
import numpy as np

from miserele import gcrlnet, rlcomp, variants, base


def is_indep(G, indset):
    s = set(indset)
    for u, v in G.edges:
        if u in s and v in s:
            return False
    return True


class MISProblemRL(rlcomp.MISRL, variants.SimplifyCompsLeavesDoms, variants.BoundMaximalMatching, base.MISProblemBase):
    pass


if __name__ == "__main__":

    parser = gcrlnet.GCRLNet.parser()
    parser.add_argument("--actions", default=1, type=int, help="Number of actions per vertex: 1, 2, 4.")
    parser.add_argument("--penalty", default=2.0, type=float, help="Log2 penalty per missed MIS vertex.")
    parser.add_argument("--train_fraction", default=0.5, type=float,
                        help="Train on a fraction of evaluated nets (speedup).")
    parser.add_argument("--min_net_vertices", default=10, type=int,
                        help="Minimal graph order to run the network on (otherwise maxdeg is used).")
    args = parser.parse_args()

    # Construct the network
    net = gcrlnet.GCRLNet(args)
    net.construct_pre()
    net.construct(v_dims=[2, 4, 8, 16, 16], e_dims=[2, 4, 8, 16, 16], g_dims=[4, 8, 16, 16, 32])
    net.construct_post()
    net.tee.start()

    all_missed = []
    all_perf = []

    for ep in range(args.epochs):
        G = nx.generators.random_graphs.gnp_random_graph(random.randint(30, 40), 0.15)

        rate = args.learning_rate
        if ep >= args.epochs * 0.4:
            rate *= 0.1
        if ep >= args.epochs * 0.7:
            rate *= 0.1

        rl_p = MISProblemRL(net, G, lower_bound=0)
        rl_p.rate = rate
        rl_res = rl_p.expand()
        best_p = variants.MISProblemBest(G, lower_bound=0)
        best_res = best_p.expand()
        missed = best_res.opt - rl_res.opt

        print("ep {:5}, N {:2}, MIS {:2} (RL missed {:2}), RL {:5} vs Best {:5} states".format(
            ep, G.order(), best_res.opt, missed, rl_res.states, best_res.states))
        all_missed.append(missed)
        all_perf.append(rl_res.states / best_res.states)
        net.compute({
            net.states_best: best_res.states,
            net.states_rl: rl_res.states,
            net.missed: missed,
            net.g_n: G.order()},
            ret=[net.test_summaries])

    print()
    print("Net evaluation and train counts by graph order:")
    print("\n".join("{:3}: {:6} {:36} {:6} {:36}".format(
        o,
        s.eval, "*" * int(2 * np.log2(1 + s.eval)),
        s.train, "*" * int(2 * np.log2(1 + s.train)))
        for o, s in sorted(net.stats.items())))

    print()
    print("Average in last 10% epochs: missed {:.4f}, states (RL/Best) {:.4f}".format(
        np.mean(all_missed[-(ep // 10):]), np.mean(all_perf[-(ep // 10):])))
