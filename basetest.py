#!/usr/bin/env python3

import networkx as nx
import networkx.algorithms.approximation

import miserele

if __name__ == '__main__':
    for G in [
        nx.generators.line.line_graph(nx.grid_graph([4, 4])),
        nx.generators.random_graphs.gnm_random_graph(20, 60, seed=42),
        nx.generators.random_graphs.gnm_random_graph(25, 80, seed=43),
        nx.generators.random_graphs.gnm_random_graph(25, 110, seed=45),
        nx.generators.random_graphs.gnm_random_graph(30, 90, seed=44),
        nx.generators.line.line_graph(nx.grid_graph([5, 4])),
        nx.generators.random_graphs.gnm_random_graph(45, 200, seed=46),
        nx.generators.random_graphs.gnm_random_graph(60, 230, seed=47),
        nx.generators.random_graphs.gnm_random_graph(70, 300, seed=47),
        miserele.planted_mis_graph(50, 15, 150, seed=5),
        miserele.planted_mis_graph(50, 15, 250, seed=6),
        miserele.planted_mis_graph(50, 15, 350, seed=7),
        miserele.planted_mis_graph(70, 20, 300, seed=1),
        miserele.planted_mis_graph(70, 20, 400, seed=2),
        miserele.planted_mis_graph(70, 20, 450, seed=3),
    ]:
        mi = nx.algorithms.approximation.independent_set.maximum_independent_set(G)
        print("\nGraph:", G.order(), G.size(), len(mi))
        if G.order() <= 30:
            print("MISProblemBase         ", miserele.MISProblemBase(G, None).expand())
        if G.order() <= 45:
            print("MISProblemSimple       ", miserele.MISProblemSimple(G, None).expand())
            print("MISProblemSimple [lb]  ", miserele.MISProblemSimple(G, 0).expand())
            print("MISProblemSimple [lb+] ", miserele.MISProblemSimple(G, len(mi)).expand())
        if G.order() <= 100:
            print("MISProblemMed          ", miserele.MISProblemMed(G, None).expand())
            print("MISProblemMed [lb]     ", miserele.MISProblemMed(G, 0).expand())
            print("MISProblemMed [lb+]    ", miserele.MISProblemMed(G, len(mi)).expand())
            print("MISProblemBest         ", miserele.MISProblemBest(G, None).expand())
            print("MISProblemBest [lb]    ", miserele.MISProblemBest(G, 0).expand())
            print("MISProblemBest [lb+]   ", miserele.MISProblemBest(G, len(mi)).expand())
