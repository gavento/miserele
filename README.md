# Maximum Independent SEt via REinforcement LEarning

Depends on [tnetbase](https://github.com/gavento/tnetbase) for utilities and
deep learning parts.

## Scripts and experiments

* `basetest.py` - Testing script
* `hardnessplot.py`, `speedplot.py` - Used to plot `imgs/` (need to be modified)
* `graphrl.py` - GraphConvNet-based reinforcement learning of branching vertices.
  Run with `--epochs 1000` for reasonable results.
  Run with `--actions 4` for heuristic MIS computation.
