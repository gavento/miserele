#!/usr/bin/env python3

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

import miserele


def sample_log_states(n, p, samples=5, planted=None):
    vals = []
    for _i in range(samples):
        if planted is not None:
            other_edges = max(int(p * 0.5 * (n ** 2 - planted * (n / planted) ** 2)), 0)  # p ~ inter-clique
            # other_edges = max(int(p * 0.5 * n ** 2 - planted * 0.5 * (n / planted) ** 2), 0) # p ~ total
            G = miserele.planted_mis_graph(n, planted, other_edges)
        else:
            G = nx.generators.random_graphs.gnp_random_graph(n, p)
        r = miserele.MISProblemBest(G, 0).expand()
        vals.append(r.states)
        print(n, p, r)
    return vals


if __name__ == '__main__':
    sizes = np.arange(5, 86, 5)
    dens = np.arange(0.1, 0.61, 0.025)
    vals = np.zeros((len(sizes), len(dens)))
    for si, s in enumerate(sizes):
        for di, d in enumerate(dens):
            # if d < s * 0.90:
                vals[si, di] = np.log2(np.median(sample_log_states(s, d, samples=4, planted=None)))
    fig, ax = plt.subplots()
    im = ax.imshow(vals)
    ax.set_xticklabels(np.round(dens, 3))
    ax.set_yticklabels(sizes)
    ax.set_xticks(np.arange(len(dens)))
    ax.set_yticks(np.arange(len(sizes)))
    ax.set_xlabel("p")
    ax.set_ylabel("n")
    ax.figure.colorbar(im, ax=ax)
    plt.title("$log_2(branching\\ states)$")
    fig.tight_layout()
    plt.show()
